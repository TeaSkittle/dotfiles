alias lock="bash scripts/i3lock.sh"
alias sudo="sudo "
alias emacs="emacs -nw --no-splash"
alias ineedhelp="lynx scripts/helpme.html"