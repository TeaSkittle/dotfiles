# TeaSkittle's Dotfiles

My simple dotfiles for a net install of Debian.
After cloning the repo, run install.sh

![screenshot](https://gitlab.com/TeaSkittle/dotfiles/raw/master/screenshot.png)