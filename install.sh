#!/bin/bash

echo "Make sure dotfiles repo is now in /home/'username'/"
echo "If directory is correct, you are then good to run :)"
echo "run as root."
echo " "

# sudo
echo "enter username(not 'root'): "
read username
apt-get install sudo
adduser $username sudo

# packages
apt-get install -y xorg git wicd \
pulseaudio alsa-utils feh gcc make \
build-essential vim firefox-esr  \
xfonts-terminus neofetch apt-file \
libncurses-dev htop python-pip lynx \
i3lock scrot libxinerama-dev \
libxft-dev compton emacs
apt-get update

# st
git clone https://git.suckless.org/st
wget https://st.suckless.org/patches/alpha/st-alpha-0.8.2.diff
cd /home/$username/dotfiles/st
patch -p1 < /home/$username/dotfiles/st-alpha-0.8.2.diff
make && make clean install
cp config.def.h config.h
sed -i -e 's/Liberation Mono/Terminus/g' config.h
make && make clean install
cd /home/$username/dotfiles

# dwm
apt-get source dwm
rm *.gz
rm *.dsc
wget https://dwm.suckless.org/patches/fullgaps/dwm-fullgaps-6.2.diff
cd /home/$username/dotfiles/dwm-6.1
patch -p1 < /home/$username/dotfiles/dwm-fullgaps-6.2.diff
make && make clean install
cp config.def.h config.h
sed -i -e 's/Mod1Mask/Mod4Mask/g' config.h
sed -i -e 's/monospace/Terminus/g' config.h
sed -i -e 's/x-terminal-emulator/st/g' config.h
make && make clean install

# bashrc
echo 'rm -rf ~/Desktop' >> /home/$username/.bashrc
echo 'rm -rf ~/Templates' >> /home/$username/.bashrc
echo 'rm -rf ~/Public' >> /home/$username/.bashrc

# emacs
echo ';;Cutsom Settings' >> /home/$username/.emacs
echo "(add-to-list 'custom-theme-load-path \"~/.emacs.d/themes\")" >> /home/$username/.emacs
echo '(global-hl-line 1)' >> /home/$username/.emacs
echo '(global-linum-mode t)' >> /home/$username/.emacs
echo "(setq ring-bell-function 'ignore)" >> /home/$username/.emacs
echo '(tool-bar-mode -1)' >> /home/$username/.emacs
echo '(menu-bar-mode -1)' >> /home/$username/.emacs

# Cleanup
cd /home/$username/dotfiles
rm README.md screenshot.png dwm-fullgaps-6.2.diff st-alpha-0.8.2.diff
rm -rf .git
mv lain.png /home/$username/
mv scripts /home/$username/
mv dwm-6.1 /home/$username/
mv st /home/$username/
mv .* /home/$username
echo "switch to user with su 'username'"
echo "then cd /home/'username' and run: startx"