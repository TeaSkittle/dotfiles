#! /bin/bash 

dte(){
  dte="$(date +"%A, %B %d | %l:%M%p")"
  echo "$dte"
}

mem(){
  mem=`free | awk '/Mem/ {printf "%d MiB/%d MiB\n", $3 / 1024.0, $2 / 1024.0 }'`
  echo "$mem"
}

cpu(){
  read cpu a b c previdle rest < /proc/stat
  prevtotal=$((a+b+c+previdle))
  sleep 0.5
  read cpu a b c idle rest < /proc/stat
  total=$((a+b+c+idle))
  cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
  echo "$cpu% cpu"
}

bat(){
  upower --dump | grep "percentage" | head -1 | tr -s [:space:] | tr -d "percentage:"
}

while true; do
     xsetroot -name "$(bat) batt | $(cpu) | $(mem) | $(dte)"
     sleep 5s    # Update time every five seconds
done &
